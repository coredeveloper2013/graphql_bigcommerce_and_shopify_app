<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () 
{
    return view('welcome');
});

Route::prefix('bigcommerce-qraphgl')->group(function ()
{
    Route::get('/get-all-product', 'ProductController@getAllProductFromBigcommerce');
    Route::get('/get-product-with-search', 'ProductController@getProductWithSearchFromBigcommerce');

    Route::get('/get-all-category', 'ProductController@getAllCategoriesFromBigcommerce');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
