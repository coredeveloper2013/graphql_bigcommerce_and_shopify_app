<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;

class ProductController extends Controller
{
    public function getAllProductFromBigcommerce()
    {
    	$store = "iuloebcvw9";

        $body = '{"channel_id": 1, "expires_at": 1602288000, "allowed_cors_origins": ["http://127.0.0.1:8000"]}';
    	
        $token_response = (new Client())->request('post', 'https://api.bigcommerce.com/stores/'.$store.'/v3/storefront/api-token', [
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Auth-Client' => 'fnr6004k0w3twl42j68elj0my9umzfl',
                'X-Auth-Token' => 's7mc4crzbn5he5ifecq4koib7gt3153'
            ],
            'body' => $body // TODO : pass json
        ]);

        $authorize_token = json_decode($token_response->getBody()->getContents());
        $authorize_token = $authorize_token->data->token;

        $query = '{"query": "query ProductInformation{site{products{edges{node{id,name,sku}}}}}"}';

		$response = (new Client())->request('post', 'https://store-'.$store.'.mybigcommerce.com/graphql', [
		'headers' => 
        [
		  'Authorization' => 'Bearer '.$authorize_token,
          'Content-Type' => 'application/json'
		],
		'body' => $query
		]);

		return response()->json(json_decode($response->getBody()->getContents()));
    }

    public function getProductWithSearchFromBigcommerce(Request $request)
    {
        /* URL: /bigcommerce-qraphgl/get-product-with-search?product_search=''&page=1&limit=10 */
        
        $product_search = isset($request->product_search) ? $request->product_search : '';
        $page = isset($request->page) ? $request->page : '';
        $limit = isset($request->limit) ? $request->limit : '';

        $store = "iuloebcvw9";

        $tags = "include_fields=id,name,sku,price,weight,description,total_sold";
        $query = $tags."";
        if($product_search != '')
        {
            $query = $query."&name:like=".$product_search;
        }
        if($page != '')
        {
            $query = $query."&page=".$page;
        }
        if($limit != '')
        {
            $query = $query."&limit=".$limit;
        }

        $response = (new Client())->request('get', 'https://api.bigcommerce.com/stores/'.$store.'/v3/catalog/products?'.$query, [
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Auth-Client' => 'fnr6004k0w3twl42j68elj0my9umzfl',
                'X-Auth-Token' => 's7mc4crzbn5he5ifecq4koib7gt3153'
            ]
        ]);

        return response()->json(json_decode($response->getBody()->getContents()));
    }

    public function getAllCategoriesFromBigcommerce()
    {
        $store = "iuloebcvw9";

        $body = '{"channel_id": 1, "expires_at": 1602288000, "allowed_cors_origins": ["http://127.0.0.1:8000"]}';
    	
        $token_response = (new Client())->request('post', 'https://api.bigcommerce.com/stores/'.$store.'/v3/storefront/api-token', [
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Auth-Client' => 'fnr6004k0w3twl42j68elj0my9umzfl',
                'X-Auth-Token' => 's7mc4crzbn5he5ifecq4koib7gt3153'
            ],
            'body' => $body // TODO : pass json
        ]);

        $authorize_token = json_decode($token_response->getBody()->getContents());
        $authorize_token = $authorize_token->data->token;
        
        $query = '{"query": "query CategoryTree3LevelsDeep{site{categoryTree{name,path,entityId}}}"}';

		$response = (new Client())->request('post', 'https://store-'.$store.'.mybigcommerce.com/graphql', [
		'headers' => 
        [
		  'Authorization' => 'Bearer '.$authorize_token,
          'Content-Type' => 'application/json'
		],
		'body' => $query
		]);

		return response()->json(json_decode($response->getBody()->getContents()));
    }
}